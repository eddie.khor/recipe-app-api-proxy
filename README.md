# Recipe App API Proxy

NGINX proxy app for our recipe API

## Usage

### Environment Variables

- `LISTEN_PORT` - Port to listen to (default: `8000`)
- `APP_HOST` - Hostname of the app to forward the request to (default: `app`)
- `APP_PORT` - Port of the app to forward the request to (default: `9000`)
